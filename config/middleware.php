<?php

use Slim\App;
use Slim\Middleware\ErrorMiddleware;
use Selective\BasePath\BasePathMiddleware;
use App\Middleware\ErrorHandlerMiddleware;
use App\Middleware\HttpExceptionMiddleware;
use Selective\Validation\Middleware\ValidationExceptionMiddleware;

return function (App $app) {
    // Parse json, form data and xml
    $app->addBodyParsingMiddleware();

    // Add the Slim built-in routing middleware
    $app->addRoutingMiddleware();

    $app->add(BasePathMiddleware::class);

    $app->add(HttpExceptionMiddleware::class);
    $app->add(ValidationExceptionMiddleware::class);

    $app->add(ErrorHandlerMiddleware::class);

    // Catch exceptions and errors
    $app->add(ErrorMiddleware::class);
};
