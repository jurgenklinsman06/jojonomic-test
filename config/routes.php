<?php

use Slim\App;

return function (App $app) {
    $app->get('/', \App\Action\HomeAction::class)->setName('home');
    $app->post('/users/', \App\Action\User\UserCreateAction::class)->setName('create-user');
    $app->get('/users/', \App\Action\User\UserListAction::class)->setName('list-user');
    $app->get('/users/{id}', \App\Action\User\UserAction::class)->setName('detail-user');
    $app->delete('/users/{id}', \App\Action\User\UserDeleteAction::class)->setName('delete-user');
    $app->post('/users/{id}', \App\Action\User\UserEditAction::class)->setName('edit-user');

    $app->post('/companies/', \App\Action\Company\CompanyCreateAction::class)->setName('create-company');
    $app->get('/companies/', \App\Action\Company\CompanyListAction::class)->setName('list-company');
    $app->get('/companies/{id}', \App\Action\Company\CompanyAction::class)->setName('detail-company');
    $app->delete('/companies/{id}', \App\Action\Company\CompanyDeleteAction::class)->setName('delete-company');
    $app->post('/companies/{id}', \App\Action\Company\CompanyEditAction::class)->setName('edit-company');
};
