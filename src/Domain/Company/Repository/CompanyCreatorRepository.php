<?php

namespace  App\Domain\Company\Repository;

use PDO;

final class CompanyCreatorRepository
{
    private $connection;

    public function __construct(PDO $connection)
    {
        $this->connection = $connection;
    }

    public function insertCompany(array $user): int
    {
        $data = [
            'name' => $user['name'],
            'address' => $user['address']
        ];

        $sql = "INSERT INTO companies SET 
                name=:name, 
                address=:address;";

        $this->connection->prepare($sql)->execute($data);

        return (int)$this->connection->lastInsertId();
    }
}
