<?php

namespace  App\Domain\Company\Repository;

use App\Exception\HttpException;
use App\Exception\HttpSpecializedException;
use PDO;

final class CompanyListRepository
{
    /**
     * @var PDO The database connection
     */
    private $connection;

    /**
     * Constructor.
     *
     * @param PDO $connection The database connection
     */
    public function __construct(PDO $connection)
    {
        $this->connection = $connection;
    }

    public function companyList(): array
    {
        $sql = "SELECT * FROM companies";
        $statement = $this->connection->prepare($sql);
        $statement->execute();
        $result = $statement->fetchAll() ?? [];

        return $result;
    }

    public function company($id)
    {
        $sql = "SELECT * FROM companies WHERE id=:id";
        $statement = $this->connection->prepare($sql);
        $statement->execute([":id" => $id]);
        $result = $statement->fetch();

        return $result;
    }

    public function deleteCompany($id)
    {
        $sql = "DELETE FROM companies WHERE id=:id";
        $statement = $this->connection->prepare($sql);

        return $statement->execute([":id" => $id]);
    }

    public function editCompany($data, $id)
    {
        $sql = "UPDATE companies SET
                name=:name, 
                address=:address WHERE id=:id";

        $statement = $this->connection->prepare($sql);

        $data = [
            ':id' => $id,
            ':name' => $data['name'],
            ':address' => $data['address']
        ];

        return $statement->execute($data);
    }
}