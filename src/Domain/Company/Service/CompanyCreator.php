<?php

namespace App\Domain\Company\Service;

use App\Domain\Company\Repository\CompanyCreatorRepository;
use Selective\Validation\Exception\ValidationException;
use Selective\Validation\ValidationResult;

/**
 * Service.
 */
final class CompanyCreator
{
    private $repository;

    public function __construct(CompanyCreatorRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Create a new user.
     *
     * @param array $data The form data
     *
     * @return int The new user ID
     */
    public function createCompany(array $data): int
    {
        $this->validateNewCompany($data);

        $companyId = $this->repository->insertCompany($data);

        return $companyId;
    }

    private function validateNewCompany(array $data): void
    {
        $validationResult = new ValidationResult();

        if (empty($data['name'])) {
            $validationResult->addError('name', 'Company name required');
        }

        if (empty($data['address'])) {
            $validationResult->addError('address', 'Company address required');
        }

        if ($validationResult->fails()) {
            throw new ValidationException('Please check your input', $validationResult);
        }
    }
}
