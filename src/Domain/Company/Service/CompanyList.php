<?php

namespace App\Domain\Company\Service;

use App\Domain\Company\Repository\CompanyListRepository;
use Selective\Validation\Exception\ValidationException;
use Selective\Validation\ValidationResult;

final class CompanyList
{
    private $repository;

    public function __construct(CompanyListRepository $repository)
    {
        $this->repository = $repository;
    }

    public function list(): array
    {
        return $this->repository->companyList();
    }

    public function company($id)
    {
        return $this->repository->company($id);
    }

    public function delete($id)
    {
        return $this->repository->deleteCompany($id);
    }

    public function edit($data, $id)
    {
        $this->validateCompany($data);
        return $this->repository->editCompany($data, $id);
    }

    private function validateCompany(array $data): void
    {
        $validationResult = new ValidationResult();

        if (empty($data['name'])) {
            $validationResult->addError('name', 'First name required');
        }

        if (empty($data['address'])) {
            $validationResult->addError('address', 'ID company required');
        }

        if ($validationResult->fails()) {
            throw new ValidationException('Please check your input', $validationResult);
        }
    }
}