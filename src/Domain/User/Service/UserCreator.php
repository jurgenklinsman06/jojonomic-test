<?php

namespace App\Domain\User\Service;

use App\Domain\User\Repository\UserCreatorRepository;
use Selective\Validation\Exception\ValidationException;
use Selective\Validation\ValidationResult;

/**
 * Service.
 */
final class UserCreator
{
    /**
     * @var UserCreatorRepository
     */
    private $repository;

    /**
     * The constructor.
     *
     * @param UserCreatorRepository $repository The repository
     */
    public function __construct(UserCreatorRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Create a new user.
     *
     * @param array $data The form data
     *
     * @return int The new user ID
     */
    public function createUser(array $data): int
    {
        // Input validation
        $this->validateNewUser($data);

        // Insert user
        $userId = $this->repository->insertUser($data);

        return $userId;
    }

    /**
     * Input validation.
     *
     * @param array $data The form data
     *
     * @throws ValidationException
     *
     * @return void
     */
    private function validateNewUser(array $data): void
    {
        $validationResult = new ValidationResult();

        // Here you can also use your preferred validation library

        if (empty($data['first_name'])) {
            $validationResult->addError('first_name', 'First name required');
        }

        if (empty($data['company_id'])) {
            $validationResult->addError('company_id', 'ID company required');
        }

        if (empty($data['email'])) {
            $validationResult->addError('email', 'Email required');
        } elseif (filter_var($data['email'], FILTER_VALIDATE_EMAIL) === false) {
            $validationResult->addError('email', 'Invalid email address');
        }

        if ($validationResult->fails()) {
            throw new ValidationException('Please check your input', $validationResult);
        }
    }
}
