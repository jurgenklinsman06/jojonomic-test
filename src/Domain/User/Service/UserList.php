<?php

namespace App\Domain\User\Service;

use App\Domain\User\Repository\UserListRepository;
use Selective\Validation\Exception\ValidationException;
use Selective\Validation\ValidationResult;

final class UserList
{
    private $repository;

    public function __construct(UserListRepository $repository)
    {
        $this->repository = $repository;
    }

    public function list(): array
    {
        return $this->repository->userList();
    }

    public function user($id)
    {
        return $this->repository->user($id);
    }

    public function delete($id)
    {
        return $this->repository->deleteUser($id);
    }

    public function edit($data, $id)
    {
        $this->validateUser($data);
        return $this->repository->editUser($data, $id);
    }

    private function validateUser(array $data): void
    {
        $validationResult = new ValidationResult();

        // Here you can also use your preferred validation library

        if (empty($data['first_name'])) {
            $validationResult->addError('first_name', 'First name required');
        }

        if (empty($data['company_id'])) {
            $validationResult->addError('company_id', 'ID company required');
        }

        if (empty($data['email'])) {
            $validationResult->addError('email', 'Email required');
        } elseif (filter_var($data['email'], FILTER_VALIDATE_EMAIL) === false) {
            $validationResult->addError('email', 'Invalid email address');
        }

        if ($validationResult->fails()) {
            throw new ValidationException('Please check your input', $validationResult);
        }
    }
}
