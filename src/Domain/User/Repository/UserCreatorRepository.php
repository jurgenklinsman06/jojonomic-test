<?php

namespace  App\Domain\User\Repository;
use PDO;

final class UserCreatorRepository{
    /**
     * @var PDO The database connection
     */
    private $connection;

    /**
     * Constructor.
     *
     * @param PDO $connection The database connection
     */
    public function __construct(PDO $connection)
    {
        $this->connection = $connection;
    }

    public function insertUser(array $user): int
    {
        $row = [
            'first_name' => $user['first_name'],
            'last_name' => $user['last_name'],
            'email' => $user['email'],
            'account' => $user['account'] ?? null,
            'company_id' => $user['company_id']
        ];

        $sql = "INSERT INTO users SET 
                first_name=:first_name, 
                last_name=:last_name, 
                email=:email,
                account=:account, 
                company_id=:company_id;";

        $this->connection->prepare($sql)->execute($row);

        return (int)$this->connection->lastInsertId();
    }

}