<?php

namespace  App\Domain\User\Repository;

use App\Exception\HttpException;
use App\Exception\HttpSpecializedException;
use PDO;

final class UserListRepository
{
    /**
     * @var PDO The database connection
     */
    private $connection;

    /**
     * Constructor.
     *
     * @param PDO $connection The database connection
     */
    public function __construct(PDO $connection)
    {
        $this->connection = $connection;
    }

    public function userList(): array
    {
        $sql = "SELECT * FROM users";
        $statement = $this->connection->prepare($sql);
        $statement->execute();
        $result = $statement->fetchAll() ?? [];

        return $result;
    }

    public function user($id)
    {
        $sql = "SELECT * FROM users WHERE id=:id";
        $statement = $this->connection->prepare($sql);
        $statement->execute([":id" => $id]);
        $result = $statement->fetch();

        return $result;
    }

    public function deleteUser($id)
    {
        $sql = "DELETE FROM users WHERE id=:id";
        $statement = $this->connection->prepare($sql);

        return $statement->execute([":id" => $id]);
    }

    public function editUser($data, $id)
    {
        $sql = "UPDATE users SET
                first_name=:first_name, 
                last_name=:last_name, 
                email=:email,
                account=:account, 
                company_id=:company_id WHERE id=:id;";

        $statement = $this->connection->prepare($sql);

        $data = [
            ':id' => $id,
            ':first_name' => $data['first_name'],
            ':last_name' => $data['last_name'],
            ':email' => $data['email'],
            ':account' => $data['account'] ?? null,
            ':company_id' => $data['company_id']
        ];

        return $statement->execute($data);
    }
}
