<?php

namespace App\Action\Company;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use App\Response\ApiResponse;
use App\Domain\Company\Service\CompanyList;

final class CompanyAction
{
    private $company;
    private $apiResponse;

    public function __construct(CompanyList $company, ApiResponse $apiResponse)
    {
        $this->company = $company;
        $this->apiResponse = $apiResponse;
    }

    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, $args): ResponseInterface
    {
        $result = $this->company->company($args['id']);

        if (!$result) {
            return $this->apiResponse
                ->json($response, null, 'RESOURCE_NOT_FOUND')
                ->withStatus(400);
        }

        return $this->apiResponse
            ->json($response, $result)
            ->withStatus(200);
    }
}
