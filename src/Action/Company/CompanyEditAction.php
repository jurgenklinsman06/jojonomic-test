<?php

namespace App\Action\Company;

use App\Domain\Company\Service\CompanyList;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use App\Response\ApiResponse;

final class CompanyEditAction
{
    private $companyList;
    private $apiResponse;

    public function __construct(CompanyList $companyList, ApiResponse $apiResponse)
    {
        $this->companyList = $companyList;
        $this->apiResponse = $apiResponse;
    }

    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, $args): ResponseInterface
    {
        $data = (array)$request->getParsedBody();

        $result = $this->companyList->edit($data, $args['id']);

        if (!$result) {
            return $this->apiResponse
                ->json($response, null, 'RESOURCE_NOT_FOUND')
                ->withStatus(400);
        }

        return $this->apiResponse
            ->json($response)
            ->withStatus(204);
    }
}
