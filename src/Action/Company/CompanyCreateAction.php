<?php

namespace App\Action\Company;

use App\Domain\Company\Service\CompanyCreator;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use App\Response\ApiResponse;

final class CompanyCreateAction
{
    private $companyCreator;
    private $apiResponse;

    public function __construct(CompanyCreator $companyCreator, ApiResponse $apiResponse)
    {
        $this->companyCreator = $companyCreator;
        $this->apiResponse = $apiResponse;
    }

    public function __invoke(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface
    {
        $data = (array)$request->getParsedBody();

        $userId = $this->companyCreator->createCompany($data);

        return $this->apiResponse
            ->json($response, ['company_id' => $userId])
            ->withStatus(201);
    }
}
