<?php

namespace App\Action\Company;

use App\Domain\Company\Service\CompanyList;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use App\Response\ApiResponse;

final class CompanyListAction
{
    private $companyList;
    private $apiResponse;

    public function __construct(CompanyList $companyList, ApiResponse $apiResponse)
    {
        $this->companyList = $companyList;
        $this->apiResponse = $apiResponse;
    }

    public function __invoke(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface
    {
        return $this->apiResponse
            ->json($response, $this->companyList->list())
            ->withStatus(200);
    }
}
