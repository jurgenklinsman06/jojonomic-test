<?php

namespace App\Action\User;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use App\Response\ApiResponse;
use App\Domain\User\Service\UserList;

final class UserAction
{
    private $user;
    private $apiResponse;

    public function __construct(UserList $user, ApiResponse $apiResponse)
    {
        $this->user = $user;
        $this->apiResponse = $apiResponse;
    }

    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, $args): ResponseInterface
    {
        $result = $this->user->user($args['id']);

        if (!$result) {
            return $this->apiResponse
                ->json($response, null, 'RESOURCE_NOT_FOUND')
                ->withStatus(400);
        }

        return $this->apiResponse
            ->json($response, $result)
            ->withStatus(200);
    }
}