<?php

namespace App\Action\User;

use App\Domain\User\Service\UserList;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use App\Response\ApiResponse;

final class UserEditAction
{
    private $userList;
    private $apiResponse;

    public function __construct(UserList $userList, ApiResponse $apiResponse)
    {
        $this->userList = $userList;
        $this->apiResponse = $apiResponse;
    }

    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, $args): ResponseInterface
    {
        $data = (array)$request->getParsedBody();
        
        $result = $this->userList->edit($data, $args['id']);

        if (!$result) {
            return $this->apiResponse
                ->json($response, null, 'RESOURCE_NOT_FOUND')
                ->withStatus(400);
        }

        return $this->apiResponse
            ->json($response)
            ->withStatus(204);
    }
}
