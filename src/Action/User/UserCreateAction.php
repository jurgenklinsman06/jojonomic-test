<?php

namespace App\Action\User;

use App\Domain\User\Service\UserCreator;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use App\Response\ApiResponse;

final class UserCreateAction
{
    private $userCreator;
    private $apiResponse;

    public function __construct(UserCreator $userCreator, ApiResponse $apiResponse)
    {
        $this->userCreator = $userCreator;
        $this->apiResponse = $apiResponse;
    }

    public function __invoke(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface
    {
        $data = (array)$request->getParsedBody();

        $userId = $this->userCreator->createUser($data);

        return $this->apiResponse
            ->json($response, ['user_id' => $userId])
            ->withStatus(201);
    }
}
