<?php

namespace App\Action\User;

use App\Domain\User\Service\UserList;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use App\Response\ApiResponse;

final class UserListAction
{
    private $userList;
    private $apiResponse;

    public function __construct(UserList $userList, ApiResponse $apiResponse)
    {
        $this->userList = $userList;
        $this->apiResponse = $apiResponse;
    }

    public function __invoke(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface
    {
        return $this->apiResponse
            ->json($response, $this->userList->list())
            ->withStatus(200);
    }
}
